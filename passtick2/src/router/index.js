import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import testmm from '@/components/testmm'
//import selectbutton from '@/components/selectbutton'
import thank from '@/components/thank'
//import cardSlide from 'vue-card-slide'
import register from'@/components/register'
import test2 from '@/components/test2'
import login from '@/components/login'
import formscan from '@/components/formscan'
import profile from '@/components/profile'
import * as VueGoogleMaps from 'vue2-google-maps'
import shop from '@/components/shopInfo'
import shopAdmin from '@/components/shopAdmin'
import sentproject from '@/components/sentproject'
import chooseregister from '@/components/chooseregister'
import registershop from '@/components/registershop'
import addProject from '@/components/addProject'
import showproject from '@/components/showproject'
import adminProject from '@/components/adminProject'
import checkshop from '@/components/checkshop'
import paybill from '@/components/paybill'
import userproject from '@/components/userproject'
import notification from '@/components/notification'
import shopaccept from '@/components/shopaccept'




Vue.use(Router)

Vue.use(VueGoogleMaps,{
  load:{
    key:'AIzaSyDetpowStg9jBMbE4Izs2OeujIjFTD_mZk',
    libraries: "places"
  }
})




export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },

    {
      path: '/rank',
      name: 'rank',
      component: testmm
    },
    {
      path: '/thank',
      name: 'thank',
      component: thank
    },
    {
      path:'/register',
      name:'register',
      component: register
    },
    {
      path:'/test2',
      name:'test2',
      component: test2
    },
    {
      path:'/login',
      name:'login',
      component: login
    },
    {
      path:'/form',
      name:'form',
      component: formscan
    },
    {
      path:'/profile',
      name:'profile',
      component: profile
    },
    {
      path:'/shop',
      name:'shop',
      component: shop
    },
    {
      path:'/shopAdmin',
      name:'shopAdmin',
      component: shopAdmin
    },
    {
      path:'/sentproject',
      name:'sentproject',
      component: sentproject
    },
    {
      path:'/choose',
      name:'choose',
      component: chooseregister
    },
    {
      path:'/registershop',
      name:'registershop',
      component: registershop
    },
    {
      path:'/addProject',
      name:'addProject',
      component: addProject
    },
    {
      path:'/showproject',
      name:'showproject',
      component: showproject
    },
    {
      path:'/adminProject',
      name:'adminProject',
      component: adminProject
    },
    {
      path:'/checkshop',
      name:'checkshop',
      component: checkshop
    },
    {
      path:'/paybill',
      name:'paybill',
      component: paybill
    },
    {
      path:'/userproject',
      name:'userproject',
      component: userproject
    },
    {
      path:'/notification',
      name:'notification',
      component: notification
    },
    {
      path:'/shopaccept',
      name:'shopaccept',
      component: shopaccept
    }





  ]
})
