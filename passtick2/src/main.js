import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import firebase from 'firebase'
//import VueGraph from 'vue-graph' 
import * as VueGoogleMaps from 'vue2-google-maps'
import moment from 'moment'
//import './registerServiceWorker'

Vue.prototype.moment = moment


const firebaseConfig = {
  apiKey: "AIzaSyAdydfmSFm52_zZGTgtajvw8PfqJfAl1-o",
  authDomain: "passtick2.firebaseapp.com",
  databaseURL: "https://passtick2.firebaseio.com",
  projectId: "passtick2",
  storageBucket: "passtick2.appspot.com",
  messagingSenderId: "427787465303",
  appId: "1:427787465303:web:ec8ee8a59f572e127ae912",
  measurementId: "G-T92221YHL7"
};
firebase.initializeApp(firebaseConfig)

//const messaging = firebase.messaging()
//messaging.requestPermission()
//.then(()=>{
//console.log('Have Permission')
//return messaging.getToken()
//})
//.then(token => {
//console.log(token)
//})
//.catch(err => {
//console.log('Error Occured' + err)
//})

//messaging.onMessage((payload) => {
//console.log('teee')
//console.log(payload)
//})




Vue.config.productionTip = false




new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')

Vue.use(VueGoogleMaps, {
  load: {
    key: 'YOUR_API_TOKEN',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})